dlib (19.24.6+dfsg-1) unstable; urgency=medium

  * New upstream version 19.24.6+dfsg

 -- Pierre Gruet <pgt@debian.org>  Mon, 07 Oct 2024 14:49:09 +0200

dlib (19.24.4+dfsg-2) unstable; urgency=medium

  * Source-only upload to unstable

 -- Pierre Gruet <pgt@debian.org>  Fri, 26 Jul 2024 07:01:59 +0200

dlib (19.24.4+dfsg-1) experimental; urgency=medium

  * New upstream version 19.24.4+dfsg
  * Refreshing patches
  * Bumping SOVERSION to 19.2
  * Updating d/copyright
  * Refreshing Lintian overrides and their syntax
  * Update renamed lintian tag names in lintian overrides.

 -- Pierre Gruet <pgt@debian.org>  Sat, 22 Jun 2024 15:34:31 +0200

dlib (19.24+dfsg-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Remove retired maintainer.

 -- Boyuan Yang <byang@debian.org>  Thu, 16 May 2024 16:31:15 -0400

dlib (19.24+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.

  [ Pierre Gruet ]
  * Fixing the soversion in d/rules

  [ Steve Langasek ]
  * Fix debian/rules to not change the upstream soname.

 -- Michael Hudson-Doyle <mwhudson@debian.org>  Wed, 28 Feb 2024 21:42:25 +0000

dlib (19.24+dfsg-1) unstable; urgency=medium

  * New upstream version 19.24+dfsg
  * Refreshing patch
  * Now acknowledging repacking in the version number with the +dfsg suffix
  * Raising Standards version to 4.6.1 (no change)
  * Having the -dev package depend on libpng-dev

 -- Pierre Gruet <pgt@debian.org>  Sun, 15 May 2022 21:23:47 +0200

dlib (19.23-2) unstable; urgency=medium

  * Upload to unstable to start the auto-dlib transition

 -- Pierre Gruet <pgt@debian.org>  Fri, 06 May 2022 09:24:53 +0200

dlib (19.23-1) experimental; urgency=medium

  * New upstream version 19.23
  * Refreshing patches
  * Refreshing d/copyright
  * Bumping SOVERSION to 19.1 (Closes: #894508)
  * Adding d/gbp.conf to handle the MUT package with gbp
  * Switching to debhelper-compat 13
  * Handling xz compression instead of gzip in d/orig-models-tar.sh
  * Raising Standards version to 4.6.0:
    - Rules-Requires-Root: no
  * Adding myself as uploader
  * Installing the pkgconfig file in the -dev package
  * Fixing missing dependencies of the -dev package (Closes: #1009008)
  * Adding a Lintian override for the txt files that are put in
    usr/include/dlib in the -dev package
  * Removing an unused Lintian override in the shared lib package
  * Removing the symlink with '0' suffix
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [Hugo Lefeuvre]
  * Bump compat to 12, switch to debhelper-compat.
  * Bump Standards-Version to 4.4.0.
  * Fix incorrect tarball name in debian/orig-tar.sh.
  * Bump copyright years.

 -- Pierre Gruet <pgt@debian.org>  Mon, 02 May 2022 22:52:31 +0200

dlib (19.10-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Backport upstream fix for using cv_image.h with OpenCV 4,
    thanks to Alexandr Podgorniy. (Closes: #990676)

 -- Adrian Bunk <bunk@debian.org>  Thu, 15 Jul 2021 17:19:19 +0300

dlib (19.10-3) unstable; urgency=medium

  * Team upload.
  * Moved packaging to Debian Science team (acknowledged by Uploaders)
  * Enable building with dpkg-buildpackage -A (thanks for the patch to
    Santiago Vila <sanvila@debian.org>)
    Closes: #918567
  * Standards-Version: 4.3.0
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Thu, 17 Jan 2019 08:17:25 +0100

dlib (19.10-2) unstable; urgency=medium

  * Add missing debian/copyright entries (Closes: #895701).
  * Bump Standards-Version to 4.1.4.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 14 Apr 2018 23:15:38 -0400

dlib (19.10-1) unstable; urgency=medium

  * New upstream release (Closes: #894118).
    - Update debian/docs to match new upstream paths.
    - Rewrite and cleanup debian/rules to match new upstream build system
      decisions.
    - Update binary package name accordingly.
  * New co-maintainer (Hugo Lefeuvre).
    - Add Uploaders entry for Hugo Lefeuvre.
    - Add debian/copyright entry for Hugo Lefeuvre.
  * Add Multi-Arch support (Closes: #894617):
    - Add required Multi-Arch fields in debian/control.
    - Update debian/*.install files to match new paths.
    - Add missing Pre-Depends field.
  * Fix soft links: Generate links files at build time, add required
    debian/*.links.in files.
  * Remove useless debian/*.dirs files.
  * Build with hardening flags.
  * Migrate repository to Salsa: update Vcs-* fields in debian/control.
  * Bump compat to 11 and update debhelper dependency accordingly.
  * Bump Standards-Version to 4.1.3.
  * Bump copyright years.
  * Update links to use HTTPS protocol.
  * Run wrap-and-sort -a.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 30 Mar 2018 21:23:27 -0400

dlib (18.18-2) unstable; urgency=medium

  * Add the static library to the dev package. Closes: #836676.
  * Add Multi-Arch:foreign to libdlib-data control. Closes: #847990.
  * Standards-Version: updated to 3.9.8

 -- Séverin Lemaignan <severin@guakamole.org>  Mon, 12 Dec 2016 22:03:37 +0000

dlib (18.18-1) unstable; urgency=low

  * Initial release. (Closes: #800531)

 -- Séverin Lemaignan <severin@guakamole.org>  Thu, 14 Jan 2016 09:54:28 +0000
